class CommentsController < ApplicationController
 before_action :set_note, only: [:index, :create, :show, :edit, :destroy]
 before_action :set_comment, only: [:index, :show, :edit, :update, :destroy]

  def index
    @comment = @note.comments.find(params[:id])
  end

 def create
   @comment = @note.comments.new(comments_params)
   @comment.save
   redirect_to root_url
 end

def edit
end

def update
    respond_to do |format|
      if @comment.update(comments_params)
        format.html { redirect_to root_url, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

 def destroy
   @comment = @note.comments.find(params[:id]).destroy
   redirect_to root_url
 end

 private

 def set_note
   	@note = Note.find(params[:note_id])
 end

 def set_comment
   @comment = Comment.find(params[:id])
 end

 def comments_params
   params.require(:comment).permit(:point)
 end
end