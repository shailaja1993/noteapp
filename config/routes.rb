Rails.application.routes.draw do
  resources :notes do  	
  	resources :comments
  end
  root 'notes#index'
  post 'notes/search', to: "notes#search", as: "search_notes"
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
